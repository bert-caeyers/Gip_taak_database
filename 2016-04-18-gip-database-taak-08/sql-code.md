
Zorg voor 1 `.sql`-tekst-bestand dat uitvoerbaar is in MySQL (Workbench)

Het bestand moet beginnen met

```
create schema gip_taak_08_AchternaamVoornaam;
use gip_taak_08_AchternaamVoornaam;
```

> Vul je eigen naam in!

Vervolgens moeten alle tabellen aangemaakt worden, b.v.

```
create table UserAccount(
    id int not null auto_increment primary key,
    name text not null,
    email text
);

create table UserInfo(
    id int not null auto_increment primary key,
    user_id int not null,
    adres text,
    postcode text
);
```

Let hierbij op of een veld NULL mag zijn of niet.

> Een foreign key mag NULL zijn als de relatie niet verplicht is!


In Mysql Workbench:

- Test 1 regel SQL uit met `ctrl-enter`
- Voer het gehele bestand uit met `shift-ctrl-enter`

Zorg dat als het schema niet bestaat (ge**DROP**ped is), je sql-bestand
correct uitvoert!

Zorg tenslotte ook nog voor het tonen van alle informatie
en enkele tests, liefst met commentaar, b.v.

```
/* toont alle info over alle users */
SELECT * 
FROM UserAccount JOIN UserInfo 
ON UserAccount.id = UserInfo.user_Id;
```

```
/* toont alle NUTTIGE info over alle users, zonder de database-id's */
SELECT name, email, adres, postcode
FROM UserAccount JOIN UserInfo 
ON UserAccount.id = UserInfo.user_Id;
```

Om na te kijken, kan de leerkracht dan gemakkelijk deze verschillende
test-queries uitvoeren met `ctrl-shift-enter`.

Denk er aan dat de naam van je database ook belangrijk is om gemakkelijk
te verbeteren te zijn: `gip_taak_08_AchternaamVoornaam`.

